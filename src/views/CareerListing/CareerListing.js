import React from 'react';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import Divider from '@mui/material/Divider';

//import Main from 'layouts/Main';
import Container from '../../components/Container';

import {  Jobs } from './components';

const CareerListing = () => {
  const theme = useTheme();
  return (<>
    {/* <Main> */}
      
      <Box bgcolor={'alternate.main'}>
        <Container maxWidth={1000}>
          <Jobs />
        </Container>
      </Box>
    {/* </Main> */}
    </>
  );
};

export default CareerListing;
