import React from 'react';
import UserDashboard from '../views/Dashboard/Dashboard';

const Dashboard = () => {
  return <UserDashboard />;
};

export default Dashboard;
